<?php
require_once '../vendor/autoload.php';

$pdf = new \Mpdf\Mpdf();
$pdf->SetImportUse();
$pagecount = $pdf->SetSourceFile('miprueba.pdf');
    for ($i=1; $i<=$pagecount; $i++) {
        $import_page = $pdf->ImportPage($i);
        $pdf->UseTemplate($import_page);

        if ($i < $pagecount)
            $pdf->AddPage();
    }
$pdf->Output();