<?php
require_once '../vendor/autoload.php';


ob_start();
require '../documento/prueba1.php';
$html= ob_get_clean();

$mpdf = new \Mpdf\Mpdf([
        'mode' => 'utf-8', 
        'format' => [297, 210], 
	'margin_bottom' => 25,
    'margin_left' => 10,
    'margin_right' => 10,
	'margin_top' =>48,
	'margin_header' => 10,
	'margin_footer' => 5,
	'orientation' => 'L'//P o L
]);
$mpdf->SetImportUse();
$mpdf->SetWatermarkImage('../documento/uni.png');
$mpdf->showWatermarkImage = true;

$mpdf->SetTitle('Prueba');
$css= file_get_contents('../documento/style.css'); // external 
$mpdf->WriteHTML($css,1);
$mpdf->WriteHTML($html,2);
$mpdf->WriteText(50,20,"Escribir por linea: Prueba Linea");

$mpdf->AddPage();

$mpdf->line(100,50,150,120);
$mpdf->line(100,50,50,120);
$mpdf->line(50,120,150,120);
$mpdf->addpage();
$pagecount = $mpdf->SetSourceFile('miprueba.pdf');
    for ($i=1; $i<=$pagecount; $i++) {
        $import_page = $mpdf->ImportPage($i);
        $mpdf->UseTemplate($import_page);

        if ($i < $pagecount)
            $mpdf->AddPage();
    }

$mpdf->Output('miprueba.pdf', 'I');

?>